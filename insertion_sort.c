//
// Created by Mykhailo Frankevich on 2/21/17.
//
#include "push_swap.h"

void	haljava_sort(t_lst **stack_a, t_lst **stack_b)
{
	t_lst	*tmp;
	int	b_val;

	while (!lst_is_sorted(*stack_a))
	{
		if ((*stack_a)->val > (*stack_a)->next->val)
			lst_swap_first(*stack_a);
		if (lst_is_sorted(*stack_a))
			return;
		tmp = *stack_a;
		b_val = (*stack_b ? (*stack_b)->val : 0);
		while (tmp->val < tmp->next->val)
			tmp = tmp->next;
		tmp = tmp->next;
		while (*stack_a != tmp)
			lst_push_from_stack(stack_a, stack_b);
		lst_rotate(stack_a);
		while (*stack_a && (*stack_a)->prev->val > (*stack_a)->val)
			lst_push_from_stack(stack_a, stack_b);
		while (*stack_b && (*stack_a)->prev->val < (*stack_b)->val)
			lst_push_from_stack(stack_b, stack_a);
		lst_rev_rotate(stack_a);
		if (!lst_is_sorted(*stack_a))
			haljava_sort(stack_a, stack_b);
		while (*stack_b && (*stack_b)->val > b_val)
			lst_push_from_stack(stack_b, stack_a);
	}
}

void	pivoting(t_lst **stack_a, t_lst **stack_b)
{
	int pivot;
	int size;

	size = lst_size(*stack_a);
	pivot = get_pivot1(*stack_a);
	while (size-- && !lst_is_sorted(*stack_a))
	{
		if ((*stack_a)->val <= pivot)
			lst_push_from_stack(stack_a, stack_b);
		else
			lst_rotate(stack_a);
	}
	return ;
}
