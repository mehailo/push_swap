//
// Created by Mykhailo Frankevich on 2/21/17.
//
#include "push_swap.h"

void	poor_sort(t_lst **stack_a, t_lst **stack_b, int size)
{
	int i;
	int max_of_pushed;
	int j;

	j = size;
	i = 0;
	max_of_pushed = MIN_INT;
	if (size < 2 || lst_is_nsorted(*stack_a, size))
		return ;
	if (size <= 3)
	{
		haljava_sort(stack_a, stack_b);
	}
	while (!lst_is_nsorted(*stack_a, size))
	{
		while (!lst_is_nsorted(*stack_a, size))
		{
			if ((*stack_a)->val > (*stack_a)->next->val)
				lst_swap_first(*stack_a);
			while (!lst_is_nsorted(*stack_a, size) || lst_is_num_smaller_in_n(*stack_a, max_of_pushed, size))
			{
				if ((i && j != size) && ((*stack_a)->val < (*stack_a)->prev->val || average_of_n_next(*stack_a, 5) < (*stack_a)->val))
				{
					lst_push_from_stack(stack_a, stack_b);
					i++;
				}
				else if (*stack_a && (*stack_a)->val > (*stack_a)->next->val)
				{
					max_of_pushed = (max_of_pushed < (*stack_a)->val ? (*stack_a)->val : max_of_pushed);
					if ((*stack_a)->val > (*stack_a)->prev->val)
						lst_rotate(stack_a);
					else
					{
						lst_push_from_stack(stack_a, stack_b);
						i++;
					}
					lst_push_from_stack(stack_a, stack_b);
					size--;
					i++;
				}
				else
					lst_rotate(stack_a);
				size--;
			}
			rev_poor_sort(stack_b, stack_a, i);
			while (j > size || (*stack_a)->prev->prev->val > (*stack_a)->prev->val)
			{
				if (!*stack_b || ((*stack_a)->prev->val > (*stack_b)->val && (*stack_a)->val > (*stack_a)->prev->val) || (size == 0 && (*stack_a)->prev->val < (*stack_a)->val))
					lst_rev_rotate(stack_a);
				else if ((*stack_a)->val > (*stack_b)->val)
					lst_push_from_stack(stack_b, stack_a);
				else
					lst_rev_rotate(stack_a);
				size++;
			}
		}
	}
}

void	rev_poor_sort(t_lst **stack_a, t_lst **stack_b, int size)
{
	int i;
	int min_of_pushed;
	int j;

	j = size;
	i = 0;
	min_of_pushed = MAX_INT;
	if (size < 2 || lst_is_nrev_sorted(*stack_a, size))
		return ;
	while (!lst_is_nrev_sorted(*stack_a, size))
	{
		while (!lst_is_nrev_sorted(*stack_a, size))
		{
			if ((*stack_a)->val < (*stack_a)->next->val)
				lst_swap_first(*stack_a);
			while (!lst_is_nrev_sorted(*stack_a, size) || lst_is_num_bigger_in_n(*stack_a, min_of_pushed, size))
			{
				if ((i && j != size) && ((*stack_a)->val > (*stack_a)->prev->val || average_of_n_next(*stack_a, 5) > (*stack_a)->val))
				{
					lst_push_from_stack(stack_a, stack_b);
					i++;
				}
				else if (*stack_a && (*stack_a)->val < (*stack_a)->next->val)
				{
					min_of_pushed = (min_of_pushed > (*stack_a)->val ? (*stack_a)->val : min_of_pushed);
					if ((*stack_a)->val < (*stack_a)->prev->val)
						lst_rotate(stack_a);
					else
					{
						lst_push_from_stack(stack_a, stack_b);
						i++;
					}
					lst_push_from_stack(stack_a, stack_b);
					size--;
					i++;
				}
				else
					lst_rotate(stack_a);
				size--;
			}
			poor_sort(stack_b, stack_a, i);
			while (j > size)
			{
				if (!*stack_b || ((*stack_a)->prev->val < (*stack_b)->val && (*stack_a)->val < ((*stack_a)->prev->val)) || (size == 0 && (*stack_a)->prev->val > (*stack_a)->val))
					lst_rev_rotate(stack_a);
				else if ((*stack_a)->val < (*stack_b)->val)
					lst_push_from_stack(stack_b, stack_a);
				else
					lst_rev_rotate(stack_a);
				size++;
			}
		}
	}
}

void	pivoting_3way(t_lst **stack_a, t_lst **stack_b)
{
	int pivot1;
	int pivot2;
	int size;

	size = lst_size(*stack_a);
	get_pivot2(*stack_a, &pivot1, &pivot2);
	while (size-- > 0 && !lst_is_sorted(*stack_a))
	{
		if ((*stack_a)->val <= pivot1)
		{
			lst_push_from_stack(stack_a, stack_b);
			if ((*stack_a)->val > pivot2)
			{
				lst_rotate_both(stack_a, stack_b);
				size--;
			}
			else
				lst_rotate(stack_b);
		}
		else if ((*stack_a)->val <= pivot2)
			lst_push_from_stack(stack_a, stack_b);
		else
			lst_rotate(stack_a);
	}
	return ;
}



void 	get_pivot2(t_lst *stack_a, int *pivot1, int *pivot2)
{
	int *array;
	int size;

	size = lst_size(stack_a);
	array = lst_to_array(stack_a);
	gnome_sort(array, size);
	*pivot1 = array[PIVOT1_RANGE];
	*pivot2 = array[PIVOT2_RANGE];
	free(array);
	return ;
}