#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int     ft_check(int *mas, int nb, int count)
{
	int i;

	i = 0;
	while (i < count)
	{
		if (mas[i] == nb)
			return (0);
		i++;
	}
	return (1);
}

void    ft_random(min, max, count)
{
	int *mas;
	int i;
	int nb;

	mas = (int *)malloc(sizeof(int) * count);
	srand(time(NULL));
	max = max - min;

	i = 0;
	while (i < count)
	{
		nb = min + rand() % max;
		if (ft_check(mas, nb, count))
		{
			mas[i++] = nb;
			printf("%d ", mas[i - 1]);
		}
	}


}

int main(int argc, char **argv)
{
	int min;
	int max;
	int count;

	if (argc == 4)
	{
		min = atoi(argv[1]);
		max = atoi(argv[2]);
		count = atoi(argv[3]);
		ft_random(min, max, count);
	}
}